import { IGNORE } from './constants';
import { promisify } from 'util';
import Zipzip from '@x82-softworks/zip';
import childProcess from 'child_process';
import fse from 'fs-extra';
import glob from 'globby';
import path from 'path';
const STD_DATE = new Date(666),
    GLOB_PATH_SEP = '/',
    SDK_REGEX = /[/|\\]aws-sdk[/|\\]?/;
const childExec = promisify(childProcess.exec);


const getAllNpm = async (root, opts = {}) => {

    const output = await childExec('npm ls --parseable --all' + (opts.optimize ? ' --prod' : ''),
        {
            cwd: root
        });

    let folders = output.stdout.split(/\r?\n/)
        .slice(1).filter(item => !!item);


    let exists = await fse.exists(path.join(root, 'node_modules', '.bin'));
    if (exists) {
        folders.push(path.join(root, 'node_modules', '.bin'));
    }
    return folders;
};

function isFile(relPath) {
    return relPath[relPath.length - 1] !== GLOB_PATH_SEP;
}

const addToZip = async (zipper, root, relpath) => {
    //all zippers are shitting the bed on metadata , meaning sha checking is useless
    //thus this hack is necessary to ignore folders as metadata is stored with them with the 
    //current date time meaning successive zips return different hashes
    if (isFile(relpath)) {

        let buffer = await fse.readFile(path.join(root, relpath));

        await zipper.file(relpath, buffer,
            {
                //arbitrary date
                date: STD_DATE
            });
    } else {
        zipper.folder(relpath,
            {
                //arbitrary date
                date: STD_DATE
            });
    }
};

/**
 * Adds the src files in the repo to the zip object
 * @param {Object} opts 
 * @param {Object} zipper 
 * @param {String} root 
 */
const addSrc = async (opts, zipper, root) => {

    const relPaths = await glob(opts.include,
        {
            // Explicitly ignore the node_modules
            ignore: [].concat(opts.exclude).concat('node_modules/**'),
            mark: true,
            sort: true,
            cwd: root
        });

    await Promise.all(relPaths.map(rel => addToZip(zipper, root, rel)));
};

const addDeps = async (opts, zipper, root) => {

    const modulePaths = await getAllNpm(root, opts.optimize);
    await Promise.all(modulePaths.map(async (rootPath) => {
        //Edge case for nested modules in the case of scoped packages
        // node_modules/moduleName
        let relPath = rootPath.slice(root.length);

        if (relPath[relPath.length - 1] !== path.sep) {
            relPath = relPath.slice(1);
        }

        let modulePath = relPath.slice('node_modules'.length + 1),
            //Check if we are dealing with a scoped package
            isScoped = modulePath[0] === '@',
            pathCheckIndex = isScoped ? modulePath.indexOf(path.sep) + 1 : 0,
            isNested = modulePath.indexOf(path.sep, pathCheckIndex) !== -1;


        //Don't bundle aws-sdk as it's inbuilt to the lambda platform
        if (!opts.includeAWSSDK && SDK_REGEX.exec(rootPath)) {
            return;
        }
        if (isNested) {
            return;
        }
        let relPaths;
        try {
            relPaths = await glob(relPath + '/**',
                {
                    cwd: root,
                    mark: true,
                    sort: true,
                    ignore: opts.exclude
                });
        } catch (err) {
            console.error(err);
        }

        return Promise.all(relPaths.map(rel => addToZip(zipper, root, rel)));
    }));
};

/**
 * Bundles up a lambda
 * @method bundle
 * @param  {Object} opts
 * @return {Promise}              
 */
export const bundle = async (opts) => {
    opts = {
        exclude: IGNORE,
        includeAWSSDK: false,
        include: ['**'],
        compressionLevel: 6,
        root: '.',
        ...opts
    };

    const zipper = new Zipzip();
    let root = opts.root;
    if (!path.isAbsolute(root)) {
        root = path.join(process.cwd(), root);
    }

    await Promise.all([
        addSrc(opts, zipper, root),
        addDeps(opts, zipper, root)
    ]);
    return await zipper.generateAsync(
        {
            compression: 'DEFLATE',
            compressionOptions:
            {
                level: opts.compressionLevel
            },
            type: 'nodebuffer',
            platform: 'UNIX'
        });

};
