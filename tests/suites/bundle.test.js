import { bundle } from '../../src';
import fse from 'fs-extra';
import path from 'path';
describe('Bundle', function () {
    it('should be able to bundle itself', async () => {

        let built = await bundle({
            include: ['package.json', 'src/**'],
            root: path.join(__dirname, '../../')
        });
        await fse.writeFile(path.join(__dirname, './bundle.zip'), built);
    });
});