#!/usr/bin/env node

const {bundle} = require('../dist');
const fsExtra = require('fs-extra');
const {IGNORE} = require('../dist/constants');
const {version} = require('../package.json');
const { program } = require('commander');


program.version(version);
process.title = 'AWS Lambda Bundler';

function collect(value, previous) {
    return previous.concat([value]);
  }


program.command('bundle <SOURCE_ROOT>')
  .description('Bundles a project into a zip.')
  .option('-o, --output <OUTPUT_PATH>','Where to output the zip file','./lambda.zip')
  .option('-i, --include <GLOB>', 'An included glob', collect, [])
  .option('-c, --compression <LEVEL>', 'Zip compression level to use',6)
  .option('-di, --defaultInclude','Disables the default includes')
  .option('-de, --defaultExclude','Disables the default excludes')
  .option('-e, --exclude <GLOB>', 'An excluded glob', collect, [])
  .option('-q, --quiet', 'Quiet mode', false)
  .option('-op, --optimize', 'Optimizes file size by omitting non prod deps ', true)
  .option('-s, --sdkInclusion', 'Whether to include aws-sdk',false)
  .action((source,opts) => {
    
    let include = opts.include;
    let exclude = opts.exclude;
    let includeBase = [];

    if (!opts.defaultInclude) {
        includeBase = ['package.json','dist/**'];
    }

    if (!opts.defaultExclude) {
        exclude = exclude.concat(IGNORE);
    }
    bundle({
        root:source,
        ignore:exclude,
        include:includeBase.concat(include),
        compressionLevel: opts.compression,
        includeAWSSDK:!!opts.sdkInclusion
    })
    .then(output=> {
        return fsExtra.outputFile(opts.output, output);
    })
    .then(()=>{
        if (!opts.quiet) {
            console.log('Bundled successfully');
        }
    })
    .catch(err=>{
        console.error(err);
    });
  });

program.parse(process.argv);