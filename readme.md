# @x82-softworks/aws-lambda-bundler

* A simple tool for optimal lambda bundling. Purposefully excludes aws-sdk from the bundle as it is usually the largest size.

It should also automatically exclude peer, optional and dev dependencies so only the smallest bundle is created. 



### Library Usage 

```javascript
import  {bundle} from '@x82-softworks/aws-lambda-bundler';

const zipBuffer = await bundle({
    include: ['package.json', 'src/**'],
    root: path.join(__dirname,'toZip')
}); 
```


### Command line usage

```bash
$ aws-lambda-bundler . 
# To get help
$ aws-lambda-bundler --help
```

The following optional values are exposed 

program.command('bundle <SOURCE_ROOT>')
  .description('Bundles a project into a zip.')
  .option('-o, --output <OUTPUT_PATH>','Where to output the zip file','./lambda.zip')
  .option('-i, --include <GLOB>', 'An included glob', collect, [])
  .option('-c, --compression <LEVEL>', 'Zip compression level to use',6)
  .option('-di, --defaultInclude','Disables the default includes')
  .option('-de, --defaultExclude','Disables the default excludes')
  .option('-e, --exclude <GLOB>', 'An excluded glob', collect, [])
  .option('-q, --quiet', 'Quiet mode', false)
   .option('-op, --optimize', 'Optimizes file size by omitting non prod deps ', true)
  .option('-s, --sdkInclusion', 'Whether to include aws-sdk',false)
